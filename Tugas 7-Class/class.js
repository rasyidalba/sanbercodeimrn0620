// Tugas 7 - Class


// Soal 1 - Animal Class

// Release 0

class Animal{
    constructor(name){
        this.name = name
        this._legs = 4
        this.cold_blooded = false
    }

    get legs (){
        return this._legs;
    }
    set legs(x){
        return this._legs = x;
    }
}

var sheep = new Animal("shaun")

console.log(sheep.name)
console.log(sheep.legs)
console.log(sheep.cold_blooded)
console.log("\n")

// Release 1

class Ape extends Animal{
    constructor(name){
        super(name)
    }

    yell(){
        return "Auooo" 
    }
}

class Frog extends Animal{
    constructor(name){
        super(name)
    }

    jump(){
        return "hop hop"
    }
}

var sungokong = new Ape("kera sakti")
sungokong.legs = 2
console.log(sungokong.name)
console.log(sungokong.legs)
console.log(sungokong.cold_blooded)
console.log(sungokong.yell())
console.log("\n")

var kodok = new Frog("buduk")
console.log(kodok.name)
console.log(kodok.legs)
console.log(kodok.cold_blooded)
console.log(kodok.jump())
console.log("\n")

// Soal 2 - Function to Class

class Clock{
  constructor({template}){
    this.template = template
    this.timer
  }
  
    render() {
      var date = new Date();
  
      var hours = date.getHours();
      if (hours < 10) hours = '0' + hours;
  
      var mins = date.getMinutes();
      if (mins < 10) mins = '0' + mins;
  
      var secs = date.getSeconds();
      if (secs < 10) secs = '0' + secs;
  
      var output = this.template
        .replace('h', hours)
        .replace('m', mins)
        .replace('s', secs);
  
      console.log(output);
    }
  
    stop() {
        clearInterval(this.timer);
    };
  
    start() {
      this.render();
      this.timer = setInterval(this.render.bind(this), 1000);
    };
  
  }
  
  var clock = new Clock({template: 'h:m:s'});
  clock.start(); 