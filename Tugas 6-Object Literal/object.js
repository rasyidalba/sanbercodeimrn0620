// Tugas 6 - Object Literal


// Soal 1 - Array to Object

function arrayToObject(arr){
    var now = new Date()
    var thisYear = now.getFullYear()
    for(var i=0;i<2;i++){
    var object = {  firstName : arr[i][0],
                    lastName : arr[i][1],
                    gender : arr[i][2],
                    age : thisYear - arr[i][3]
                 }
    if(!arr[i][3] || object.age<1){
        object.age = "Invalid Birth Year"
    }
    console.log(i+1+". " +object.firstName+" "+object.lastName+":")
    console.log(object)
    }
    return "";
}

var people = [ ["Bruce", "Banner","male", 1975],["Natasha","Romanoff","female"]]
var people2 = [ ["Tony", "Stark", "male", 1980], ["Pepper", "Pots", "female", 2023] ]

console.log(arrayToObject(people))
console.log(arrayToObject(people2))
console.log("\n")

// Soal 2 - Shopping Time

function shoppingTime(memberId, money){
    if(!memberId){
        return " Mohon maaf, toko X hanya berlaku untuk member saja"
    } else if( money<50000){
        return "Mohon maaf, uang tidak cukup"
    } else {
        var i = 0;
        var object = { memberId : memberId,
                       money : money,
                       listPurchased : [],
                       changeMoney : money
                     }
            if(money>=1500000){
                object.listPurchased[i] = "Sepatu Stacattu"
                money= money-1500000;
                i= i +1;
            }  if(money>= 500000){
                object.listPurchased[i] = "Baju Zoro"
                money = money-500000;
                i= i +1;
            }  if(money>=250000){
                object.listPurchased[i] = "Baju H&N"
                money= money-250000;
                i= i +1;
            }  if(money>=175000){
                object.listPurchased[i] = "Sweater Uniklooh"
                money=money-175000;
                i= i +1;
            }  if(money>=50000){
                object.listPurchased[i] = "Casing Handphone"
                money = money-50000;
                i= i +1;
            }
        object.changeMoney = money;
        return object;
    }
}

console.log(shoppingTime('1820RzKrnWn08', 2475000));
console.log(shoppingTime('82Ku8Ma742', 170000));
console.log(shoppingTime('', 2475000));
console.log(shoppingTime('234JdhweRxa53', 15000));
console.log(shoppingTime());
console.log("\n");

// Soal 3 - Naik Angkot

function naikAngkot(arrPenumpang){
    rute = ['A','B','C','D','E','F'];
    var ongkos = 0;
    var array = []
   if(arrPenumpang.length == 0){return array;} else{
    for(var i=0;i<2;i++){
        var object = {penumpang : arrPenumpang[i][0],
                      naikDari : arrPenumpang[i][1],
                      tujuan : arrPenumpang[i][2],
                      bayar : ongkos}
        var asal = rute.indexOf(arrPenumpang[i][1])
        var tujuan = rute.indexOf(arrPenumpang[i][2])
        ongkos = tujuan-asal;
        object.bayar = ongkos*2000;
        array[i] = object;
    }}
    return array;
}

console.log(naikAngkot([["Dimitri",'B','F'], ['Icha','A','B']]));
console.log(naikAngkot([]));