// Tugas 9 - ES6


// Soal 1 - Mengubah fungsi menjadi fungsi arrow

const golden= () => {
    console.log("this is golden!!")
}

golden()
console.log("\n")

// Soal 2 - Sederhanakan menjadi Object literal di ES6

const newFunction = (firstName, lastName) => {
    return {
        firstName,
        lastName,
        fullName : function(){
            console.log(`${firstName} ${lastName}`)
            return
        }
    }
}

newFunction("William", "Imoh").fullName()
console.log("\n")

// Soal 3 - Destructuring

const newObject = {
    firstName: "Harry",
    lastName: "Potter Holt",
    destination: "Hogwarts React Conf",
    occupation: "Deve-wizard Avocado",
    spell: "Vimulus Renderus!!!"
}

const {firstName,lastName,destination,occupation} = newObject

console.log(firstName,lastName,destination,occupation)
console.log("\n")

// Soal 4 - Array Spreading

const west = ["Will", "Chris", "Sam", "Holly"]
const east = ["Gill", "Brian", "Noel", "Maggie"]
const combined = [...west, ...east]

console.log(combined)
console.log("\n")

// Soal 5 - Template Literals

const planet = "earth"
const view = "glass"
var before = `Lorem ${view} dolor sit amet, consectetur adipiscing elit, ${planet} do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam`

console.log(before)