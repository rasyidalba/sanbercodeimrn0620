// Tugas 3 - Looping


// Soal 1 - Looping While

var a = 2;
var a2 = 20;

while(a<=20){
    console.log(a, " - I love coding");
    a=a+2;
}
console.log("\n");
while(a2>=2){
    console.log(a2, " - I will become a mobile developer");
    a2=a2-2;
}
console.log("\n");

// Soal 2 - Looping For

for(var b=1;b<=20;b++){
    if(b%2 == 1){
        if(b%3==0){
            console.log(b, " - I Love Coding");
        }
        else{
            console.log(b, " - Santai");
        }
    }
    else{
        console.log(b, " - Berkualitas");
    }
}
console.log("\n");

// Soal 3 - Membuat Persegi Panjang #

for(var c=1;c<=4;c++){
    for(var c2=1;c2<=8;c2++){
        process.stdout.write("#");
    }
    process.stdout.write("\n");
}
console.log("\n");

// Soal 4 - Membuat Tangga

var deret = "";
var d=0;

while(d<7){
    deret= deret +"#";
    console.log(deret);
    d++;
}
console.log("\n");

// Soal 5 - Membuat Papan Catur

for(var e=1;e<=8;e++){
    if(e%2==1){
        for(var e2=1;e2<=8;e2++){
            if(e2%2==1){
                process.stdout.write(" ");
            }
            else{
                process.stdout.write("#");
            }
        }
        process.stdout.write("\n");
    }
    else{
        for(var e2=1;e2<=8;e2++){
            if(e2%2==0){
                process.stdout.write(" ");
            }
            else{
                process.stdout.write("#");
            }
        }
        process.stdout.write("\n");
    }
}