import React, { Component } from 'react'
import { View, StyleSheet,FlatList ,Platform, Text,Image, TouchableOpacity, ScrollView } from 'react-native'
import Icon from 'react-native-vector-icons/MaterialIcons';

export default class App extends Component {
    render() {
      return (
        <View style={styles.container}>
            <View style={styles.navBar}>
                <Text style={styles.textNavBar}>Profile</Text>
                <Text style={styles.textNavBar}>Logout</Text>
            </View>
            <ScrollView>
                <Image source={require('./Images/puzzle.png')} style={{width:300,height:300, alignSelf:'center'}} /> 
                <View style={styles.isiText}>
                    <Text style={styles.title}>username</Text>
                    <View style={{width:320,height:1,backgroundColor:'#033060',alignSelf:'center'}}></View>
                    <Text style={{fontSize:24, alignSelf:'flex-start', paddingLeft: 20, paddingBottom:30}}>cyril46</Text>
                    <Text style={styles.title}>skills</Text>
                    <View style={{width:320,height:1,backgroundColor:'#033060',alignSelf:'center'}}></View>
                    <Text style={styles.subTitle}>Language</Text>
                    <View style={styles.skillList}>
                        <View style={styles.skillItem}>
                            <Image source={require('./Images/js-logo.png')} style={styles.imageSkill}/>
                            <Text style={styles.skillTitle}>Basic Javascript</Text>
                            <Text style={styles.skillPercentage}>50%</Text>
                        </View>
                        <View style={styles.skillItem}>
                            <Image source={require('./Images/c-logo.png')} style={styles.imageSkill}/>
                            <Text style={styles.skillTitle}>Intermediate C++</Text>
                            <Text style={styles.skillPercentage}>75%</Text>
                        </View>
                    </View>
                    <View style={{width:320,height:1,backgroundColor:'#033060',alignSelf:'center'}}></View>
                    <Text style={styles.subTitle}>Framework/Library</Text>
                        <View style={styles.skillItem}>
                            <Image source={require('./Images/react-logo.png')} style={styles.imageSkill}/>
                            <Text style={styles.skillTitle}>Basic React Native</Text>
                            <Text style={styles.skillPercentage}>25%</Text>
                        </View>
                    <View style={{width:320,height:1,backgroundColor:'#033060', alignSelf:'center'}}></View>
                    <Text style={styles.subTitle}>Technology</Text>
                    <View style={styles.skillList}>
                        <View style={styles.skillItem}>
                            <Image source={require('./Images/android-studio-logo.png')} style={styles.imageSkill}/>
                            <Text style={styles.skillTitle}>Basic Android Studio</Text>
                            <Text style={styles.skillPercentage}>100%</Text>
                        </View>
                        <View style={styles.skillItem}>
                            <Image source={require('./Images/gitlab.png')} style={styles.imageSkill}/>
                            <Text style={styles.skillTitle}>Basic Gitlab</Text>
                            <Text style={styles.skillPercentage}>75%</Text>
                        </View>
                    </View>
                </View>
            </ScrollView>
        </View>
      )
    }
}

const styles = StyleSheet.create({
    container: {
      flex: 1
    },
    navBar:{
        height: 60,
        backgroundColor: '#033060',
        paddingHorizontal:10,
        paddingVertical:20,
        color:'white',
        flexDirection:'row',
        justifyContent:'space-between'
    },
    textNavBar:{
        fontSize:14,
        fontWeight:'bold',
        color: 'white'
    },
    isiText:{

    },
    title:{
        alignSelf:'flex-start',
        paddingLeft:20,
        color:'#888888'
    },
    subTitle:{
        fontSize:20,
        alignSelf:'flex-start',
        paddingLeft:18
    },
    skillList:{
        flexDirection:"row",
        justifyContent: 'space-around'
    },
    skillItem:{
        alignItems:'center'
    },
    skillTitle:{
        fontSize:12,
        fontWeight:'bold'
    },
    skillPercentage:{
        fontSize:12,
        color: '#888888'
    }, 
    imageSkill:{
        width:100,
        height:100,
    }
})