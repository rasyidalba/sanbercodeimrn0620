import React, { Component } from 'react'
import { View, StyleSheet,FlatList,Platform, Text,Image,Button, TouchableOpacity } from 'react-native'
import Icon from 'react-native-vector-icons/MaterialIcons';
import About from './AboutScreen';

export default class App extends Component {
    render() {
      return (
        <View style={styles.container}>
                <View style={styles.logo}>
                    <Image source={require('./Images/sanberlogo.png')} style={{width:288,height:175}} />
                    <View style={styles.isi}>
                        <View style={styles.isis}>
                            <Text style={styles.isiText}>Username/E-mail</Text>
                            <View style={styles.kolomIsi}></View>
                        </View>
                        <View style={styles.isis}>
                            <Text style={styles.isiText}>Password</Text>
                            <View style={styles.kolomIsi}></View>
                        </View>
                    </View>
                    <Button
          title="Login"
          onPress={() => this.props.navigation.navigate('About')} />
                    
                        <View style={styles.login}>
                            <Text style={styles.loginText}>Login</Text>
                        </View>
                   
                </View>
        </View>
      )
    }
}

const styles = StyleSheet.create({
    container: {
      flex: 1,
      flexDirection: 'column',
      justifyContent:'space-around',
    },
    logo:{
        alignItems:'center',
        paddingBottom:40
    },
    isi:{
        paddingTop:50,
        paddingBottom:50
    },
    isis:{
        paddingTop: 20
    },
    isiText:{
        fontSize: 12,
        color : 'black',
        alignSelf:'flex-start'  
    },
    kolomIsi:{
        height:35,
        backgroundColor:'#93BADD',
        alignSelf:'center',
        width:250
    },
    login:{
        backgroundColor: '#12508A',
        height: 28,
        width:80,
        justifyContent: 'center'
    },
    loginText:{
        fontSize: 11,
        color: 'white',
        textAlign: 'center'
    }
  });