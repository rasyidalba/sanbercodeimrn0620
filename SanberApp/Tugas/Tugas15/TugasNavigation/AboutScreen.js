import React, { Component } from 'react'
import { View, StyleSheet,FlatList,Platform, Text,Image,Button, TouchableOpacity } from 'react-native'
import Icon from 'react-native-vector-icons/MaterialIcons';
import { StatusBar } from 'expo-status-bar';


export default class App extends Component {
    render() {
      return (
        <View style={styles.container}>
                <View style={styles.navBar}>
                    <Icon name='arrow-back' size={40} color={'white'}/>
                    
                </View>
                <View style={styles.blockProfile}>
                <Image source={require('./Images/Profile.png')} style={{width:80,height:80,borderRadius:40,alignSelf:'center'}} />
                </View>
                <View style={styles.userName}>
                  <Text style={styles.userNameText}>Cyril McKenneth</Text>
                  <View style={{width:280,height:2,backgroundColor:'black'}}></View>
                </View>
                <View style={styles.sosialMedia}>
                  <Text style={styles.titleText}>Social Media</Text>
                  <View style={styles.kolomSosialMedia}>
                      <Text style={styles.isiText}>Facebook <Text style={{fontWeight: "bold", fontSize:18}}>     Cyril McKenneth</Text></Text>
                      <View style={{width:280,height:1,backgroundColor:'black'}}></View>
                      <Text style={styles.isiText}>Twitter <Text style={{fontWeight: "bold", fontSize:18}}>         CyrilMKh46</Text></Text>
                      <View style={{width:280,height:1,backgroundColor:'black'}}></View>
                      <Text style={styles.isiText}>Instagram <Text style={{fontWeight: "bold", fontSize:18}}>    Cyril McKenneth</Text></Text>
                  </View>
                </View>
                <View style={styles.portofolio}>
                  <Text style={styles.titleText}>Portofolio</Text>
                  <View style={styles.kolomPortofolio}>
                      <Text style={styles.isiText}>Gitlab <Text style={{fontWeight: "bold", fontSize:18}}>           https://gitlab.com/cyr...</Text></Text>
                      <View style={{width:280,height:1,backgroundColor:'black'}}></View>
                      <Text style={styles.isiText}>Github <Text style={{fontWeight: "bold", fontSize:18}}>          https://github.com/cy...</Text></Text>
                  </View>
                </View>
        </View>
      )
    }
}

const styles = StyleSheet.create({
    container: {
      flex: 1
    },
    navBar: {
        height: 60,
        backgroundColor: '#033060',
        paddingHorizontal:5,
        paddingVertical:10,
        color:'white'
      },
    blockProfile:{
      height:225,
      backgroundColor: '#29A8E0',
      justifyContent:'center'
    },
    userName:{
      paddingTop:30,
      alignItems: 'center',
      paddingBottom: 50
    },
    userNameText:{
      fontSize:30,
    },
    sosialMedia:{
      paddingHorizontal:14
    },
    titleText:{
      fontSize: 16,
      color : 'black',
      alignSelf:'flex-start'  
    },
    kolomSosialMedia:{
      height:150,
      backgroundColor:'#93BADD',
      alignSelf:'center',
      alignItems:'center',
      justifyContent:'space-around',
      paddingVertical:10,
      width:330
    },
    portofolio:{
      paddingTop:20,
      paddingHorizontal:14
    },
    kolomPortofolio:{
      height:100,
      backgroundColor:'#93BADD',
      alignSelf:'center',
      alignItems:'center',
      justifyContent:'space-around',
      paddingVertical:10,
      width:330
    },
    isiText:{
      paddingHorizontal:20,
      alignSelf:'flex-start',
      fontSize:14
    }
  });