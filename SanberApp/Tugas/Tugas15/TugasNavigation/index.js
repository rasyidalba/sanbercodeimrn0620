import React from "react";
import { NavigationContainer } from "@react-navigation/native";
import { createStackNavigator } from "@react-navigation/stack";
import { createBottomTabNavigator } from "@react-navigation/bottom-tabs";
import { createDrawerNavigator } from "@react-navigation/drawer";

import Skill from './SkillScreen';
import Project from './ProjectScreen';
import Add from './AddScreen';
import About from './AboutScreen';
import Login from './LoginScreen';


const Tabs = createBottomTabNavigator();
const RootStack = createStackNavigator();
const Drawer = createDrawerNavigator();



const TabsScreen = () => (
    <Tabs.Navigator>
        <Tabs.Screen name="SkillScreen" component={Skill} />
        <Tabs.Screen name="ProjectScreen" component={Project} />
        <Tabs.Screen name="AddScreen" component={Add} />
      </Tabs.Navigator>
)

const RootScreen = () => (
    <RootStack.Navigator>
        <RootStack.Screen name="Login" component={Login} />
        <RootStack.Screen name="About" component={About} />
      </RootStack.Navigator>
)

export default () => (
    <NavigationContainer>
      <Drawer.Navigator>
      <Drawer.Screen name="Login" component={Login} />  
      <Drawer.Screen name="Tabs" component={TabsScreen} />
      <Drawer.Screen name="About" component={About} />
    </Drawer.Navigator>
      {}
    </NavigationContainer>
)