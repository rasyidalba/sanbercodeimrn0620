// Tugas 5 - Array


//Soal 1 (Range)



function range(a,b){
    
    if(!a || !b){
        return -1;
    }
    else  {
        var array1 = [0];
        array1[0]= a;
        if(a<b){
            for(var i=1;i<=b-a;i++){
                array1[i]=a+i;
                
            }
        } else if (a>=b) {
            for(var i=1;i<=a-b;i++){
                array1[i]=a-i;
            }
        }
        return array1;
    }
}

console.log(range(1,10));
console.log(range(1));
console.log(range(11,18));
console.log(range(54,50));
console.log(range());
console.log("\n");

// Soal 2 - Range with Step

function rangeWithStep(c,d,step){
    var array2 = [0];
    array2[0] = c;
    var temp = 0;
    if(c<d){
        while(c<=d){
            array2[temp] = c;
            c= c+step;
            temp++;
        }
    } else if (c>d){
        while(c>=d){
            array2[temp] = c;
            c= c-step;
            temp++;
        }
    }
    return array2;
}

console.log(rangeWithStep(1,10,2));
console.log(rangeWithStep(11,23,3));
console.log(rangeWithStep(5, 2, 1));
console.log(rangeWithStep(29, 2, 4));
console.log("\n");

// Soal 3 - Sum of Range

function sum(e,f,langkah){
    var array3 = [0];
    array3[0] = e;
    var temp = 0;
    if(e && !f){
        return e;
    }
    jumlah=0;
    if(!langkah){
        langkah=1;
    }
    if(e<f){
        while(e<=f){
            array3[temp] = e;
            jumlah=jumlah+array3[temp];
            e= e+langkah;
            temp++;
            
        }
    } else if (e>f){
        while(e>=f){
            array3[temp] = e;
            jumlah=jumlah+array3[temp];
            e= e-langkah;
            temp++;
        }
    }
    return jumlah;
}

console.log(sum(1,10))
console.log(sum(5, 50, 2))
console.log(sum(15,10))
console.log(sum(20, 10, 2))
console.log(sum(1))
console.log(sum())
console.log("\n");

// Soal 4 - Array Multidimensi

function dataHandling(n){
    for(var n=0;n<=3;n++){
        console.log("Nomor ID :\t"+input[n][0]);
        console.log("Nama Lengkap :\t"+input[n][1]); 
        console.log("TTL :\t"+input[0][2]+input[n][3]);
        console.log("Hobi :\t"+input[n][4]);
        console.log("\n");
    }
}

var input = [
    ["0001", "Roman Alamsyah", "Bandar Lampung", "21/05/1989", "Membaca"],
    ["0002", "Dika Sembiring", "Medan", "10/10/1992", "Bermain Gitar"],
    ["0003", "Winona", "Ambon", "25/12/1965", "Memasak"],
    ["0004", "Bintang Senjaya", "Martapura", "6/4/1970", "Berkebun"]
] 
dataHandling();
console.log("\n");

// Soal 5 - Balik Kata

function balikKata(string){
    var akhir = string.length-1;
    var temp = "";
    while(akhir>=0){
        temp=temp + string[akhir];
        akhir--;        
    }
    return temp;
}

console.log(balikKata("Kasur Rusak"));
console.log(balikKata("SanberCode"));
console.log(balikKata("Haji Ijah"));
console.log(balikKata("racecar"));
console.log(balikKata("I am Sanbers"));
console.log("\n");

// Soal 6 - Metode Array

function dataHandling2(input2){
    input2.splice(1,2, "Roman Alamsyah Elsharawy", "Provinsi Bandar Lampung");
    input2.splice(4,2, "Pria", "SMA Internasional Metro");
    console.log(input2);

    var bulan = input2[3].split("/");
    switch(bulan[1]) {
        case '01': {console.log("Januari"); break;}
        case '02': {console.log("Februari"); break;}
        case '03': {console.log("Maret"); break;}
        case '04': {console.log("April"); break;}
        case '05': {console.log("Mei"); break;}
        case '06': {console.log("Juni"); break;}
        case '07': {console.log("Juli"); break;}
        case '08': {console.log("Agustus"); break;}
        case '09': {console.log("September"); break;}
        case '10': {console.log("Oktober"); break;}
        case '11': {console.log("November"); break;}
        case '12': {console.log("Desember"); break;}
        default: {console.log("Jumlah bulan cuma ada 12!"); break;}
    }

    var date = bulan.join("-");
    bulan.sort((a, b) => b - a);
    console.log(bulan);
    console.log(date);

    var nama = input2[1];
    var name = nama.slice(0,14);
    console.log(name);
}

var input2 = ["0001", "Roman Alamsyah", "Bandar Lampung", "21/05/1989", "Membaca"];
dataHandling2(input2);

