//Tugas pekan 1 - hari ke-2  - Conditional

// Soal 1 - if-else

var nama = "Cyril";
var peran = "Werewolf";

if (nama == "" && peran == "")
    {console.log("Nama harus diisi!!")}
    else if (nama == "Cyril" && peran == "")
        {console.log("Halo "+nama, ", pilih peranmu untuk memulai game!")}
        else if (nama == "Cyril" && peran == "Penyihir")
            {console.log("Halo "+ peran +" " +nama, ", kamu dapat melihat siapa yang menjadi Werewolf!")}
            else if (nama == "Cyril" && peran == "Guard")
                {console.log("Halo "+ peran +" " +nama, ", kamu akan membantu melindungi temanmu dari serangan Werewolf!")}
                else if (nama == "Cyril" && peran == "Werewolf")
                    {console.log("Halo "+ peran +" " +nama, ", kamu akan memakan mangsa setiap malam!")}

console.log("\n");

// Soal 2 - Switch Case

var hari = 14;
var bulan = 6;
var tahun = 2000;

switch(bulan) {
    case 1: {console.log(hari,"Januari "+ tahun); break;}
    case 2: {console.log(hari,"Februari "+ tahun); break;}
    case 3: {console.log(hari,"Maret "+ tahun); break;}
    case 4: {console.log(hari,"April "+ tahun); break;}
    case 5: {console.log(hari,"Mei "+ tahun); break;}
    case 6: {console.log(hari,"Juni "+ tahun); break;}
    case 7: {console.log(hari,"Juli "+ tahun); break;}
    case 8: {console.log(hari,"Agustus "+ tahun); break;}
    case 9: {console.log(hari,"September "+ tahun); break;}
    case 10: {console.log(hari,"Oktober "+ tahun); break;}
    case 11: {console.log(hari,"November "+ tahun); break;}
    case 12: {console.log(hari,"Desember "+ tahun); break;}
    default: {console.log("Variabel bulan hanya dapat diisi dengan 1-12"); break;}
}